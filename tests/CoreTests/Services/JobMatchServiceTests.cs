using Moq;
using Core.Entities;
using Core.Interfaces;
using NUnit.Framework;
using System.Collections.Generic;
using Core.Services;
using System.Threading.Tasks;
using FluentAssertions;

namespace CoreTests.Services
{
    public class JobMatchServiceTests
    {        
        [Test]
        public async Task Job_Matches_One_Candidate_Partial()
        {
            var jobsRepoMock = new Mock<IJobsRepository>();
            var candidatesRepoMock = new Mock<ICandidatesRepository>();

            var jobId = 1;

            jobsRepoMock.Setup(jobsRepo => jobsRepo.GetJob(It.IsAny<int>()))
                .ReturnsAsync(new Job()
                {
                    JobId = jobId,
                    Company = "SomeCompany",
                    Name = "SomeName",
                    Skills = new List<Skill>
                    {
                        new Skill { Ranking = 1, SkillName = "Top Skill"},
                        new Skill { Ranking = 2, SkillName = "Mid Skill"},
                        new Skill { Ranking = 3, SkillName = "Low Skill"}
                    }
                });

            candidatesRepoMock.Setup(candidatesRepo => candidatesRepo.GetCandidatesWithSkills(It.IsAny<IEnumerable<Skill>>()))
                .ReturnsAsync(new List<Candidate>
                {
                    new Candidate
                    {
                        CandidateId = 5,
                        Name = "SomeCandidate",
                        Skills = new List<Skill>
                        {
                            new Skill { Ranking = 1, SkillName = "Mid Skill"},
                            new Skill { Ranking = 2, SkillName = "Low Skill"}
                        }
                    },
                    new Candidate
                    {
                        CandidateId = 6,
                        Name = "SomeCandidate",
                        Skills = new List<Skill>
                        {
                            new Skill { Ranking = 1, SkillName = "High Skill"}
                        }
                    }
                });

            var expected = new Candidate
            {
                CandidateId = 5,
                Name = "SomeCandidate",
                Skills = new List<Skill>
                        {
                            new Skill { Ranking = 1, SkillName = "Mid Skill"},
                            new Skill { Ranking = 2, SkillName = "Low Skill"}
                        }
            };

            var sut = new JobMatchService(jobsRepoMock.Object, candidatesRepoMock.Object);

            var actual = await sut.MatchCandidateToJob(jobId);
            actual.Should().NotBeNull();
            actual.Should().BeEquivalentTo(expected);
        }

        [Test]
        public async Task Job_Matches_No_Candidate()
        {
            var jobsRepoMock = new Mock<IJobsRepository>();
            var candidatesRepoMock = new Mock<ICandidatesRepository>();

            var jobId = 1;

            jobsRepoMock.Setup(jobsRepo => jobsRepo.GetJob(It.IsAny<int>()))
                .ReturnsAsync(new Job()
                {
                    JobId = jobId,
                    Company = "SomeCompany",
                    Name = "SomeName",
                    Skills = new List<Skill>
                    {
                        new Skill { Ranking = 1, SkillName = "Top Skill"},
                        new Skill { Ranking = 2, SkillName = "Mid Skill"},
                        new Skill { Ranking = 3, SkillName = "Low Skill"}
                    }
                });

            candidatesRepoMock.Setup(candidatesRepo => candidatesRepo.GetCandidatesWithSkills(It.IsAny<IEnumerable<Skill>>()))
                .ReturnsAsync(new List<Candidate>
                {});

            var sut = new JobMatchService(jobsRepoMock.Object, candidatesRepoMock.Object);

            var actual = await sut.MatchCandidateToJob(jobId);

            actual.Should().BeNull();
        }
    }
}