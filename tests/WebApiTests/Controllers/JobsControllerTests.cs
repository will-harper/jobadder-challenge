﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using Web.Controllers;
using Web.Extensions;
using Web.Models;

namespace WebApiTests.Controllers
{
    public class JobsControllerTests
    {
        [Test]
        public async Task Get_Job_Returns_OK()
        {
            var jobsRepoMock = new Mock<IJobsRepository>();
            var jobsMatchService = new Mock<IJobMatchService>();
            var jobId = 1;

            var job = new Job
            {
                JobId = jobId,
                Name = "JobName",
                Company = "CompanyName",
                Skills = new List<Skill>
                         {
                             new Skill { Ranking = 1, SkillName = "High Skill"}
                         }
            };

            var expected = job.AsDto();

            jobsRepoMock.Setup(i => i.GetJob(It.IsAny<int>()))
                .ReturnsAsync(job);

            var sut = new JobsController(jobsRepoMock.Object, jobsMatchService.Object);

            var actual = await sut.GetJob(jobId);

            actual.Should().BeOfType<OkObjectResult>();
            ((OkObjectResult)actual).Value.Should().BeOfType<JobDto>();

            var actualJobDto = (JobDto)((OkObjectResult)actual).Value;

            actualJobDto.Should().BeEquivalentTo(expected);
        }

        [Test]
        public async Task Get_Job_Invalid_Id_Returns_404()
        {
            var jobsRepoMock = new Mock<IJobsRepository>();
            var jobsMatchService = new Mock<IJobMatchService>();

            jobsRepoMock.Setup(i => i.GetJob(It.IsAny<int>()))
                .ReturnsAsync((Job)null);

            var sut = new JobsController(jobsRepoMock.Object, jobsMatchService.Object);

            var actual = await sut.GetJob(1);

            actual.Should().BeOfType<NotFoundObjectResult>();

        }

        [Test]
        public async Task Get_Jobs_Returns_OK()
        {
            var jobsRepoMock = new Mock<IJobsRepository>();
            var jobsMatchService = new Mock<IJobMatchService>();

            var jobs = new List<Job>
            {
                new Job
                {
                    JobId = 1,
                    Name = "JobName1",
                    Company = "CompanyName1",
                    Skills = new List<Skill>
                             {
                                 new Skill { Ranking = 1, SkillName = "High Skill"}
                             }
                },
                new Job
                {
                    JobId = 2,
                    Name = "JobName2",
                    Company = "CompanyName2",
                    Skills = new List<Skill>
                             {
                                 new Skill { Ranking = 1, SkillName = "Mid Skill"}
                             }
                }
            };

            var expected = jobs.Select(job => job.AsDto());

            jobsRepoMock.Setup(i => i.GetJobs())
                .ReturnsAsync(jobs);

            var sut = new JobsController(jobsRepoMock.Object, jobsMatchService.Object);

            var actual = await sut.GetJobs();

            actual.Should().BeOfType<OkObjectResult>();
            ((OkObjectResult)actual).Value.Should().BeAssignableTo<IEnumerable<JobDto>>();

            var actualJobsDto = (IEnumerable<JobDto>)((OkObjectResult)actual).Value;

            actualJobsDto.Should().BeEquivalentTo(expected);
        }
    }
}
