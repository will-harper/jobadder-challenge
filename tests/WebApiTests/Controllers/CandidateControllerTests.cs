﻿using System;
using System.Threading.Tasks;
using Core.Interfaces;
using NUnit.Framework;
using Moq;
using Core.Entities;
using Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Web.Extensions;
using Web.Models;
using System.Linq;
using FluentAssertions;

namespace WebApiTests.Controllers
{
    public class CandidateControllerTests
    {
        [Test]
        public async Task Get_Candidate_OK()
        {
            var requestCandidateid = 1;

            var candidatesRepoMock = new Mock<ICandidatesRepository>();
            var candidate = new Candidate
            {
                CandidateId = requestCandidateid,
                Name = "SomeCandidate",
                Skills = new List<Skill>
                        {
                            new Skill { Ranking = 1, SkillName = "High Skill"}
                        }
            };
            var expected = candidate.AsDto();

            candidatesRepoMock.Setup(candidatesRepo => candidatesRepo.GetCandidate(It.IsAny<int>()))
                .ReturnsAsync(candidate);

            var sut = new CandidatesController(candidatesRepoMock.Object);

            var actual = await sut.GetCandidate(requestCandidateid);


            actual.Should().BeOfType<OkObjectResult>();
            ((OkObjectResult)actual).Value.Should().BeOfType<CandidateDto>();

            var actualCandidateDto = (CandidateDto)((OkObjectResult)actual).Value;

            actualCandidateDto.Should().BeEquivalentTo(expected);

        }

        [Test]
        public async Task Get_Candidates_Returns_OK()
        {
            var candidatesRepoMock = new Mock<ICandidatesRepository>();
            var candidate = new List<Candidate> {
                new Candidate
                {
                    CandidateId = 1,
                    Name = "SomeCandidate",
                    Skills = new List<Skill>
                            {
                                new Skill { Ranking = 1, SkillName = "High Skill"}
                            }
                },
                new Candidate
                {
                    CandidateId = 2,
                    Name = "SomeCandidate",
                    Skills = new List<Skill>
                            {
                                new Skill { Ranking = 1, SkillName = "Mid Skill"}
                            }
                }
            };

            var expectedCandidateDtoResults = candidate.Select(candidate => candidate.AsDto());

            candidatesRepoMock.Setup(candidatesRepo => candidatesRepo.GetCandidates())
                .ReturnsAsync(candidate);

            var sut = new CandidatesController(candidatesRepoMock.Object);

            var actual = await sut.GetCandidates();

            actual.Should().BeOfType<OkObjectResult>();
            ((OkObjectResult)actual).Value.Should().BeAssignableTo<IEnumerable<CandidateDto>>();

            var resultAsCandidateDtos = ((IEnumerable<CandidateDto>)((OkObjectResult)actual).Value).ToList();

            resultAsCandidateDtos.Should().BeEquivalentTo(expectedCandidateDtoResults);
        }
        [Test]
        public async Task Get_Candidate_Invalid_Id_Returns_404()
        {
            var candidatesRepoMock = new Mock<ICandidatesRepository>();

            candidatesRepoMock.Setup(candidatesRepo => candidatesRepo.GetCandidate(It.IsAny<int>()))
                .ReturnsAsync((Candidate)null);

            var sut = new CandidatesController(candidatesRepoMock.Object);

            var actual = await sut.GetCandidate(1);

            actual.Should().BeOfType<NotFoundObjectResult>();
        }
    }
}
