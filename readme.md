# JobAdder Coding Test - Will Harper

## How to Run

From the root of the project directory where the solution file is located the project can be run
by executing the command 
```
dotnet run --launch-profile Web --project ./src/Web/Web.csproj
```
This should open up a page at http://localhost:5000 that will show the website. The page
does not open, then open a new browser window and enter that address in the URL bar.

## How to Test

### Dotnet

The dotnet tests can be run from the root directory with the command.

```
dotnet test
```

### Angular

The angular tests can be run from within the project directory, which can be completed
with the following set of commands.

```
cd src/Web/ui
npm install
npm test 
```