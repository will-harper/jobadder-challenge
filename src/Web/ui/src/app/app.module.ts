import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { JobListComponent } from './jobs/components/job-list/job-list.component';
import { JobComponent } from './jobs/components/job/job.component';
import { JobsComponent } from './jobs/jobs.component';
import { CandidateComponent } from './candidates/components/candidate/candidate.component';
import { CandidateListComponent } from './candidates/components/candidate-list/candidate-list.component';
import { CandidatesComponent } from './candidates/candidates.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatCardModule} from '@angular/material/card';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatDividerModule} from '@angular/material/divider';
import {MatButtonModule} from '@angular/material/button';
import {MatChipsModule} from '@angular/material/chips';
import { HttpClientModule } from '@angular/common/http';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { ErrorComponent } from './error/error.component';

@NgModule({
  declarations: [
    AppComponent,
    JobListComponent,
    JobComponent,
    JobsComponent,
    CandidateComponent,
    CandidateListComponent,
    CandidatesComponent,
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatSidenavModule,
    MatCardModule,
    MatDividerModule,
    MatButtonModule,
    MatChipsModule,
    MatProgressSpinnerModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
