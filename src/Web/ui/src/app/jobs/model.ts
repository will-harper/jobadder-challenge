import { Skill } from '../shared/models';

export interface Job {
  jobId: number;
  name: string;
  company: string;
  skills: Skill[];
}
