import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Candidate } from 'src/app/candidates/models';
import { Job } from '../model';

@Injectable({
  providedIn: 'root'
})
export class JobsService {

  constructor(private http: HttpClient) { }

  getJobs(): Observable<Job[]> {
    return this.http.get<Job[]>('/api/jobs');
  }

  getMostQualifiedCandidate(jobId: number): Observable<Candidate> {
    return this.http.get<Candidate>(`/api/jobs/${jobId}/candidate`);
  }
}
