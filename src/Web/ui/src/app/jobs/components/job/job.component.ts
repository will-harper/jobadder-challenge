import { Candidate } from './../../../candidates/models';
import { Component, Input, OnInit } from '@angular/core';
import { Job } from '../../model';
import { JobsService } from '../../services/jobs.service';

@Component({
  selector: 'app-job',
  templateUrl: './job.component.html',
  styleUrls: ['./job.component.css']
})
export class JobComponent {
  @Input() job!: Job;

  mostQualifiedCandidateOpen = false;
  mostQualifiedCandidate: Candidate | undefined;

  constructor(private jobService: JobsService) { }

  openCloseMostQualifiedCandidate(): void {
    this.mostQualifiedCandidateOpen = !this.mostQualifiedCandidateOpen;
    if (!this.mostQualifiedCandidate) {
      this.getMostQualifiedCandidate();
    }
  }

  getMostQualifiedCandidate(): void {
    this.jobService.getMostQualifiedCandidate(this.job.jobId).subscribe(data => this.mostQualifiedCandidate = data);
  }
}
