import { TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { Candidate } from 'src/app/candidates/models';
import { JobsService } from 'src/app/jobs/services/jobs.service';

import { JobComponent } from './job.component';

class MockJobsService {
  getMostQualifiedCandidate(jobId: number): Observable<Candidate> {
    const candidate: Candidate = { candidateId: 1, name: 'Test Candidate', skills: [{ranking: 1, skillName: 'High Skill'}]};
    return of(candidate);
  }
}

describe('JobComponent', () => {
  let component: JobComponent;
  let jobsService: JobsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ JobComponent ],
      providers: [
        JobComponent,
        { provide: JobsService, useClass: MockJobsService }
      ]
    });

    component = TestBed.inject(JobComponent);
    component.job = { jobId: 1, company: 'Company', name: 'Name', skills: [{ranking: 1, skillName: 'skill'}]};
    jobsService = TestBed.inject(JobsService);
  });

  it('should create', () => {

    expect(component).toBeTruthy();
    expect(component.job).toBeDefined();
  });

  it('should not have a candidate after construction', () => {
    expect(component.job).toBeDefined();
    expect(component.mostQualifiedCandidate).toBeUndefined();
    expect(component.mostQualifiedCandidateOpen).toBeFalsy();
  });

  it('should have a candidate after open is clicked', () => {
    expect(component.job).toBeDefined();
    component.openCloseMostQualifiedCandidate();
    expect(component.mostQualifiedCandidate).toBeDefined();
  });
});
