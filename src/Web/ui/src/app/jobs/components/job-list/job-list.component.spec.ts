import { TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { JobsService } from 'src/app/jobs/services/jobs.service';
import { Job } from 'src/app/jobs/model';
import { JobListComponent } from './job-list.component';

class MockJobsService {
  getJobs(jobId: number): Observable<Job[]> {
    const jobs: Job[] = [
      { jobId: 1, company: 'Company1', name: 'Name1', skills: [{ranking: 1, skillName: 'skill'}]},
      { jobId: 2, company: 'Company2', name: 'Name2', skills: [{ranking: 1, skillName: 'skill'}]}
    ];

    return of(jobs);
  }
}

describe('JobListComponent', () => {

  let component: JobListComponent;
  let jobsService: JobsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ JobListComponent ],
      providers: [
        JobListComponent,
        { provide: JobsService, useClass: MockJobsService }
      ]
    });

    component = TestBed.inject(JobListComponent);
    jobsService = TestBed.inject(JobsService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
