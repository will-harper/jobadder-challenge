import { JobsService } from './../../services/jobs.service';
import { Component, OnInit } from '@angular/core';
import { Job } from '../../model';

@Component({
  selector: 'app-job-list',
  templateUrl: './job-list.component.html',
  styleUrls: ['./job-list.component.css']
})
export class JobListComponent implements OnInit {

  jobs: Job[] = [];
  pending = true;

  constructor(private jobService: JobsService) { }

  ngOnInit(): void {
    this.jobService.getJobs().subscribe((data) => {
      this.jobs = data;
      this.pending = false;
    });
  }
}
