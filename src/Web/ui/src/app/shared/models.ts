export interface Skill {
  ranking: number;
  skillName: string;
}
