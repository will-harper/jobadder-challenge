import { Component, Input, OnInit } from '@angular/core';
import { Candidate } from '../../models';

@Component({
  selector: 'app-candidate',
  templateUrl: './candidate.component.html',
  styleUrls: ['./candidate.component.css']
})
export class CandidateComponent{
  @Input() candidate!: Candidate;
}
