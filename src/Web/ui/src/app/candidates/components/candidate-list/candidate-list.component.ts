import { CandidatesService } from './../../services/candidates.service';
import { Component, OnInit } from '@angular/core';
import { Candidate } from '../../models';

@Component({
  selector: 'app-candidate-list',
  templateUrl: './candidate-list.component.html',
  styleUrls: ['./candidate-list.component.css']
})
export class CandidateListComponent implements OnInit {
  candidates: Candidate[] = [];
  pending = true;

  constructor(private candidateService: CandidatesService) { }

  ngOnInit(): void {
    this.candidateService.getCandidates().subscribe((data) => {
      this.candidates = data;
      this.pending = false;
    });
  }

}
