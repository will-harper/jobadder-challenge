import { Skill } from './../shared/models';

export interface Candidate {
  candidateId: number;
  name: string;
  skills: Skill[];
}
