﻿using System;
using System.Linq;
using Core.Entities;
using Web.Models;

namespace Web.Extensions
{
    public static class DtoExtensions
    {
        public static SkillDto AsDto(this Skill skill)
            => new SkillDto
            {
                Ranking = skill.Ranking,
                SkillName = skill.SkillName
            };

        public static JobDto AsDto(this Job job)
            => new JobDto
            {
                JobId = job.JobId,
                Company = job.Company,
                Name = job.Name,
                Skills = job.Skills.Select(i => i.AsDto())
            };

        public static CandidateDto AsDto(this Candidate job)
            => new CandidateDto
            {
                CandidateId = job.CandidateId,
                Name = job.Name,
                Skills = job.Skills.Select(i => i.AsDto())
            };
    }
}
