﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Web.Extensions;

namespace Web.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class CandidatesController : Controller
    {
        private readonly ICandidatesRepository _candidatesRepository;

        public CandidatesController(ICandidatesRepository candidatesRepository)
        {
            _candidatesRepository = candidatesRepository;
        }

        [HttpGet]
        public async Task<IActionResult> GetCandidates()
        {
            var candidates = await _candidatesRepository.GetCandidates();
            return Ok(candidates.Select(candidate => candidate.AsDto()));
        }

        [HttpGet("{candidateId:int:min(1)}")]
        public async Task<IActionResult> GetCandidate(int candidateId)
        {
            var candidate = await _candidatesRepository.GetCandidate(candidateId);

            if (candidate is null)
                return NotFound($"Candidate with candidateId: {candidateId} not found.");

            return Ok(candidate.AsDto());
        }
    }
}
