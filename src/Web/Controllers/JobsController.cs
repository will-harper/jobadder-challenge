using System.Linq;
using System.Threading.Tasks;
using Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Web.Extensions;

namespace Web.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class JobsController : Controller
    {
        private readonly IJobsRepository _jobsRepository;
        private readonly IJobMatchService _jobMatchService;        

        public JobsController(IJobsRepository jobsRepository, IJobMatchService jobMatchService)
        {
            _jobsRepository = jobsRepository;
            _jobMatchService = jobMatchService;
        }

        [HttpGet("{jobId:int:min(1)}")]
        public async Task<IActionResult> GetJob(int jobId)
        {
            var job = await _jobsRepository.GetJob(jobId);

            if (job is null)
                return NotFound($"Job with jobId: {jobId} not found.");

            return Ok(job.AsDto());
        }

        [HttpGet]
        public async Task<IActionResult> GetJobs()
        {
            var jobs = await _jobsRepository.GetJobs();
            return Ok(jobs.Select(job => job.AsDto()));
        }

        [HttpGet("{jobId:int:min(1)}/candidate")]
        public async Task<IActionResult> GetCandidatesMatchingJob(int jobId)
        {
            var matchingCandidate = await _jobMatchService.MatchCandidateToJob(jobId);

            return Ok(matchingCandidate.AsDto());
        }
    }
}