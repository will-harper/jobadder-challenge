using System;
using System.Linq;
using System.Threading.Tasks;
using Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Web.Extensions;

namespace Web.Controllers
{
    [ApiController]
    public class ErrorController : Controller
    {
        [Route("api/error")]
        public IActionResult HandleError()
        {
            return Redirect("/error");
        }
    }
}
