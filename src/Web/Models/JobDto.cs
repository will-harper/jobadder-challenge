using System.Collections.Generic;

namespace Web.Models
{
    public class JobDto
    {
        public int JobId { get; set; }
        public string Name { get; set; }
        public string Company { get; set; }
        public IEnumerable<SkillDto> Skills { get; set; }
    }
}