using System.Collections.Generic;

namespace Web.Models
{
    public class CandidateDto
    {        
        public int CandidateId { get; set; }
        public string Name { get; set; }
        public IEnumerable<SkillDto> Skills { get; set; }
    }
}