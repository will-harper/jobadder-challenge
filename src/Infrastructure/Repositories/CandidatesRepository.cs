using Core.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;
using System.Linq;

namespace Infrastructure.Repositories
{
    public class CandidatesRepository : ICandidatesRepository
    {
        private readonly JobsContext _context;

        public CandidatesRepository(JobsContext context)
        {
            _context = context;
        }
        public Task<Candidate> GetCandidate(int id)
            => Task.FromResult(_context.Candidates.FirstOrDefault(i => i.CandidateId == id));

        public Task<IEnumerable<Candidate>> GetCandidates()
            => Task.FromResult(_context.Candidates);

        public Task<IEnumerable<Candidate>> GetCandidatesWithSkills(IEnumerable<Skill> skills)
            => Task.FromResult(_context.Candidates.Where(candidate => candidate.Skills.Intersect(skills, new SkillComparer()).Any()));
    }
}