using Core.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;
using System.Linq;

namespace Infrastructure.Repositories
{
    public class JobsRepository : IJobsRepository
    {
        private readonly JobsContext _context;

        public JobsRepository(JobsContext context)
        {
            _context = context;
        }
        
        public Task<Job> GetJob(int id)
            => Task.FromResult(_context.Jobs.FirstOrDefault(i => i.JobId == id));

        public Task<IEnumerable<Job>> GetJobs()
            => Task.FromResult(_context.Jobs);
    }
}