using Microsoft.Extensions.DependencyInjection;
using Core.Interfaces;
using Infrastructure.Repositories;

namespace Infrastructure
{
    public static class ServiceCollectionExtensions
    {
        
        public static IServiceCollection AddInfrastructureServices(this IServiceCollection services)
        {
            services.AddScoped<IJobsRepository, JobsRepository>();
            services.AddScoped<ICandidatesRepository, CandidatesRepository>();
            services.AddScoped<JobsContext>();

            return services;
        }
    }
}