using System.Linq;
using System.Threading.Tasks;
using Core.Interfaces;
using Core.Entities;
using System.Collections.Generic;

namespace Core.Services
{
    public class JobMatchService : IJobMatchService
    {
        private readonly IJobsRepository _jobsRepository;
        private readonly ICandidatesRepository _candidatesRepository;

        public JobMatchService(IJobsRepository jobsRepository, ICandidatesRepository candidatesRepository)
        {
            _jobsRepository = jobsRepository;
            _candidatesRepository = candidatesRepository;
        }

        public async Task<Candidate> MatchCandidateToJob(int jobId)
        {
            var job = await _jobsRepository.GetJob(jobId);

            var candidates = await _candidatesRepository.GetCandidatesWithSkills(job.Skills);

            if (!candidates.Any())
                return null;

            return candidates.Select(candidate => new
            {
                candidate = candidate,
                skillSum = candidate.Skills.Join(job.Skills,
                                            candidate => candidate.SkillName,
                                            job => job.SkillName,
                                            (candidateSkills, jobSkills) => System.Math.Abs(candidateSkills.Ranking - jobSkills.Ranking))
            })
            .OrderByDescending(i => i.skillSum.Count())
            .ThenBy(i => i.skillSum.Sum())
            .First().candidate;
        }
    }
}