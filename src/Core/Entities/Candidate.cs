using System.Collections.Generic;

namespace Core.Entities
{
    public class Candidate
    {        
        public int CandidateId { get; set; }
        public string Name { get; set; }
        public IEnumerable<Skill> Skills { get; set; }
    }
}