using System.Collections.Generic;

namespace Core.Entities
{
    public class Skill
    {
        public int Ranking { get; set; }
        public string SkillName { get; set; }
    }

    public class SkillComparer : IEqualityComparer<Skill>
    {
        public bool Equals(Skill x, Skill y)
        {
            return x.SkillName == y.SkillName;
        }

        public int GetHashCode(Skill obj)
        {
            return obj.SkillName.GetHashCode();
        }
    }
}