using Microsoft.Extensions.DependencyInjection;
using Core.Interfaces;
using Core.Services;

namespace Core
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCoreServices(this IServiceCollection services)
        {
            services.AddScoped<IJobMatchService, JobMatchService>();

            return services;
        }
    }
}