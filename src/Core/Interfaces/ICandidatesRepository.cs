using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;

namespace Core.Interfaces
{
    public interface ICandidatesRepository
    {
        Task<Candidate> GetCandidate(int id);
        Task<IEnumerable<Candidate>> GetCandidates();
        Task<IEnumerable<Candidate>> GetCandidatesWithSkills(IEnumerable<Skill> skills);
    }
}