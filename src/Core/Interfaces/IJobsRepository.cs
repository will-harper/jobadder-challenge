using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;

namespace Core.Interfaces
{
    public interface IJobsRepository
    {
         Task<Job> GetJob(int id);
         Task<IEnumerable<Job>> GetJobs();
    }
}