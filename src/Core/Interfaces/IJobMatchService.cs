using System.Threading.Tasks;
using Core.Entities;
using System.Collections.Generic;

namespace Core.Interfaces
{
    public interface IJobMatchService
    {
        Task<Candidate> MatchCandidateToJob(int jobId);
    }
}